import React from 'react';
import './footer.css'
//images
import  twitter  from '../../images/twitter-icon.png'
import  facebook  from '../../images/facebook-icon.png'
import  instagram  from '../../images/instagram-icon.png'
import  linkedin  from '../../images/linkedin-icon.png'

function Footer() {
    return (
    <footer>
    <div className="footer__menu">
        <ul>
            <p style={{ fontWeight : "bold" }}>Site Map</p>
            <li><a href="{% url  'inicioCol'  %}">Inicio</a></li>
            <li><a href="{% url 'taxerCol'  %}">Taxer</a></li>
            <li><a href="{% url 'logerdashCol' %}">LogerDash</a></li>
            <li><a href="{% url 'blog' %}">Blog</a></li>
        </ul>
    </div>
    <div className="footer__contacto">
        <p style={{ fontWeight : "bold" }}>Oficinas</p>
        <div className="footer__contacto--description">
            <p>Matamoros 46, Barrio de Santa Inés. Las Vigas de Ramirez, Veracruz.<br/>México.</p>
            <p>Manga Terminal Marítimo, Cartagena de Indias. Colombia</p>
            <p>Teléfono: 🇲🇽 +52 1 (228)3525036  🇨🇴 +57 (300)7739057</p>
            <p>Contacto: ventas@logiety.com</p>
        </div>
    </div>

    <div className="footer__social">
        <p style={{ fontWeight : "bold" }}>Redes sociales</p>
        <div className="footer__social--icons">
            <a href="https://twitter.com/logiety" target="_blank" rel="noreferrer" /><img src={ twitter } alt="twitter" />
            <a href="https://www.instagram.com/logiety/" target="_blank" rel="noreferrer"/><img src={ instagram } alt="instagram" />
            <a href="https://www.facebook.com/logiety/" target="_blank" rel="noreferrer"/><img src={ facebook } alt="facebook" />
            <a href="https://www.linkedin.com/company/logiety/" target="_blank" rel="noreferrer"/><img src={ linkedin } alt="linkedin" />
        </div>
    </div>

    <div className="footer__copyrights">
        <p>Logiety Technology SAPI de CV. Todos los derechos reservados 2020</p>
    </div>
    </footer>
    )
}

export default Footer
