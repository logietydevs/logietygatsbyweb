import React, { useState } from 'react';
import { Link } from 'gatsby';
//Custom components
import NavLink from './components/NavLink';
//Images
import logoWhite from '../../images/logowhite.png';
import mexicoFlag from '../../images/flag-mex.png';
import colombiaFlag from '../../images/flag-col.png';
import { FaBars } from "react-icons/fa";
import { FaCaretDown } from "react-icons/fa";
//Styles
import './navbar.css';
import { motion } from 'framer-motion';


const variants = {
    visible: { 
        display: '',
    },
    hidden: { 
        display: 'none',
    },
}


const Navbar = () => {

    const [flag, setFlag] = useState('México');
    const [showModal, setShowModal] = useState("hidden");


    return (
    <nav>
    <div className="menu_xl">
        {flag === 'México' ?
        <Link to="/" ><img className="brand_image" src={ logoWhite } alt="Logiety Logo" /></Link>
        :<Link to="/colombia" ><img className="brand_image" src={ logoWhite } alt="Logiety Logo" /></Link>
        }

        <div className="menu__icon">
        { showModal === 'hidden' ?
        <button className="menu__icon" onClick={() => setShowModal('visible')}><FaBars /></button>
        :
        <button onClick={() => setShowModal('hidden')}><FaBars /></button>
        }
        </div>


        <div className="nav__options">
            <ul>

            {flag === 'México' && (
            <>
                <NavLink to="/" >Home</NavLink>
                <NavLink to="/taxer/mexico" >Taxer</NavLink>
                <NavLink to="/logerdash/mexico" >LogerDash</NavLink>
            </>
            )}
            {flag === 'Colombia' && (
            <>
                <NavLink to="/colombia" >Home</NavLink>
                <NavLink to="/taxer/colombia" >Taxer</NavLink>
                <NavLink to="/logerdash/colombia" >LogerDash</NavLink>

            </>
            )}
                <NavLink to="/blog">Blog</NavLink>

                <li className="dropdown" style={{ color : "white" }} >{ flag }
                { flag === 'México' ?
                <img src={ mexicoFlag } onClick={()=> setShowModal("hidden")} alt="Mexico flag" style={{ width : "30px", height: "24px", paddingLeft: "10px"  }} />
                :
                <img src={ colombiaFlag } onClick={()=> setShowModal("hidden")} alt="Colombia flag" style={{ width : "30px", height: "24px", paddingLeft: "10px"  }} />
                }

                { showModal === 'hidden' ?
                <motion.button
                onClick={() => setShowModal('visible')}
                whileHover={{ scale: 1.3, color: "#00ffd0", }}
                ><FaCaretDown />
                </motion.button>:
                <motion.button
                onClick={() => setShowModal('hidden')}
                whileHover={{ scale: 1.3, color: "#00ffd0", }}
                ><FaCaretDown />
                </motion.button>
                }
                </li>
            </ul>
        </div>
    </div>

   
    <motion.div className="menu"
    variants={ variants }
    initial="hidden"
    animate={ showModal }
    >
        <ul>
        {flag === 'México' && (
            <>
            <li><Link onClick={()=> setShowModal("hidden")} to="/">Home</Link></li>
            <li><Link onClick={()=> setShowModal("hidden")} to="/taxer/mexico">Taxer</Link></li>
            <li><Link onClick={()=> setShowModal("hidden")} to="/logerdash/mexico">LogerDash</Link></li>
            <li><Link onClick={()=> setShowModal("hidden")} to="/blog">Blog</Link></li>
            <li><img src={ mexicoFlag } alt="Mexico flag" style={{ width: "50px", height:"34px", opacity: "0.5" }} /></li>
            <li>
            <Link to="/colombia" onClick={() => setFlag('Colombia')}>
            <img onClick={()=> setShowModal("hidden")} src={ colombiaFlag } alt="Colombia flag" style={{ width: "50px", height: "34px" }} />
            </Link>
            </li>
            </>
            )
        }
        {flag === 'Colombia' && (
            <>
            <li><Link onClick={()=> setShowModal("hidden")} to="/colombia">Home</Link></li>
            <li><Link onClick={()=> setShowModal("hidden")} to="/taxer/colombia">Taxer</Link></li>
            <li><Link onClick={()=> setShowModal("hidden")} to="/logerdash/colombia">LogerDash</Link></li>
            <li><Link onClick={()=> setShowModal("hidden")} to="/blog">Blog</Link></li>
            <li><img src={ colombiaFlag } alt="Colombia flag" style={{ width: "50px", height:"34px", opacity: "0.5" }} /></li>
            <li>
            <Link to ="/" onClick={() => setFlag('México')}>
            <img onClick={()=> setShowModal("hidden")} src={ mexicoFlag } alt="Mexico flag" style={{ width: "50px", height: "34px" }} />
            </Link>
            </li>
            </>
            )
        }
        </ul>
    </motion.div>



    <motion.div className="country" 
    variants={ variants }
    initial="hidden"
    animate={ showModal }
    >
        <ul>
            <li>
            { flag === 'México' ?
            <Link to="/colombia" onClick={() => setFlag('Colombia') } style={{ color : "white" }}>
            <button onClick={() => setShowModal("hidden")}>
            <img src={ colombiaFlag } alt="Colombia flag" style={{ width: "40px" }}/>
            <br />Colombia
            </button>
            </Link>
            :
            <Link to="/" onClick={() => setFlag('México') } style={{ color : "white" }}>
            <button onClick={() => setShowModal("hidden")}>
            <img src={ mexicoFlag } alt="Mexico flag" style={{ width: "40px" }}/>
            <br />Mexico
            </button>
            </Link>
            }
            </li>
        </ul>
    </motion.div>
    </nav>

    )
}

export default Navbar
