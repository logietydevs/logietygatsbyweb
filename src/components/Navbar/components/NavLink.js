import React from 'react';
import { Link } from 'gatsby';

const linkStyles = {
    fontSize: "18px",
    color: 'white',
    textDecoration: 'none',
    paddingLeft: '0',
    paddingRight: '40px',
    listStyle: 'none'
}

const NavLink = ({children, to}) => (
    <Link to={ to } style={ linkStyles }>
    {children}</Link>
);


export default NavLink;