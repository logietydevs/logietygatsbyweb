import React from "react";
import PropTypes from "prop-types";
import Footer from "../components/Footer/Footer";
import Navbar from "../components/Navbar/Navbar"
 
import "./layout.css";

const Layout = ({ children }) => {
  return (
    <>
      <Navbar />
      <div>
        {children}
      </div>
      <Footer />
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
