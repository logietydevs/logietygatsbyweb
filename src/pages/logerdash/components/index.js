import React from 'react';
import { Link } from 'gatsby';
import SavingCalculatorMex from './SavingCalculatorMex';
import SavingCalculatorCol from './SavingCalculatorCol';
//images 
import tramitadores from '../../../images/tramitadores-aduaneros.png';
import tramitadoresResponsive from '../../../images/tramitadores-aduaneros-responsive.png';
import previoConLogerdash from '../../../images/previo-con-logerdash-big.png';
import previoSinLogerdash from '../../../images/previo-sin-logerdash-mobile.png';
import mobile from '../../../images/mobile.jpg';
import security from '../../../images/security.jpg';
import computer from '../../../images/computer.jpg';
import chulito from '../../../images/chulito.png'
//styles
import '../logerdash.css';
import { motion } from 'framer-motion';

function Logerdash(props) {
    return (
        <>
        <div className="hero__previo">
        <div className="hero__title--previo">
            <h1>{ props.heroTitle }</h1>
            <h2>LogerDash reduce el <b>contacto físico</b> con el manejo información en<b> línea</b> y en <b>tiempo
                real.</b></h2>
            <div className="hero__previo--image--responsive">
                <img src={ tramitadoresResponsive} alt="Agente Aduanal" />
            </div>

                <Link to="/forms/registro-logerdash"><motion.button className="green"
                whileHover={{ scale: 1.1 }}
                whileTap={{ scale: 0.9 }}
                >Activar 15 días gratis</motion.button></Link>

            <p>Compruebe los resultados de LogerDash sin costo</p>
        </div>
        <div className="hero__previo--image">
            <img src={ tramitadores } alt="Reportes de aduanas" />
        </div>
    </div>
    <div className="band__benefitsLoger">
        <div className="band__benefitsLoger--text">
            <h2>Sin inversiones en tabletas, maneje todo desde su celular</h2>
        </div>
    </div>

    <div className="process">
        <div className="process__description--noLoger">
            <h2>{ props.processAfter }</h2>
            <br/>
            <img src={ previoSinLogerdash } alt="Proceso sin logerDash" />
        </div>

        <div className="process__description--loger">
            <h2>{ props.processBefore }</h2>
            <br/>
            <img src={ previoConLogerdash } alt="Proceso con logerDash" />
            <h4>LogerDash es el sistema más avanzado para capturar <br /> y automatizar los procesos del previo
                aduanero.</h4>
        </div>
    </div>

    <div className="management">
        <div className="management__description">
            <h2>Ahorra con LogerDash desde su primer uso.</h2>
            <p>Sustituir los procesos manuales, con un previo digitalizado le brinda beneficios en su bolsillo
                como: </p>
            <p><img src={ chulito }  style={{ width: "16px", height: "16px" }} /> Aumentar productividad,
                cubriendo más operaciones con
                menos personal.</p>
            <p><img src={ chulito }  style={{ width: "16px", height: "16px" }} /> Reduce los contagios de
                COVID-19 al evitar los
                traslados entre recintos y oficinas.</p>
            <p><img src={ chulito }  style={{ width: "16px", height: "16px" }} /> Ahorros inmediatos con la
                reducción
                de hasta el 60% del tiempo de operaciones.</p>
            <p><img src={ chulito } style={{ width: "16px", height: "16px" }} /> Reducción de 40% de los
                costos de la agencia aduanal.</p>
        </div>

    {
        props.country === "México" ? <SavingCalculatorMex />
        : <SavingCalculatorCol />
    }

    </div>

    <div className="benefits">
        <div className="benefits__title">
            <h2>Captura desde el celular. Recibe y tramita desde la oficina</h2>
            <p>Conexiones en tiempo real con
                ejecutivos, tramitadores, gerentes
                y agentes aduanales</p>
        </div>
        <div className="benefits__mobile">
            <img src={ mobile } alt="telefono celular" />
        </div>
        <div className="benefits__mobile--description">
            <h2>App de Celular</h2>
            <p> + Captura la información de la previo.<br/><br/>
                + Puede crear operaciones en caso de mercancía sobrantes.<br/><br/>
                + Soporta hasta 1000 fotografías un envío.<br/><br/>
                + Puede funcionar sin datos (Offline).<br/><br/>
                + Multisesión para trabajo colaborativo entre operadores.</p>
        </div>

        <div className="benefits__security">
            <img src={ security } alt="Seguridad de la informacion" />
        </div>

        <div className="benefits__security--description">
            <h2>Seguridad</h2>
            <p> + Manejo de conexiones SSL y cifradas.<br/><br/>
                + Uso de llaves simétricas RSA 4096 bits.<br/><br/>
                + Firewall Datacenter y Servidor.<br/><br/>
                + Llaves GPG.<br/><br/>
                + Seguridad nivel A+ en SSL Labs.</p>
        </div>

        <div className="benefits__computer">
            <img src={ computer } alt="computadora" />
        </div>
        <div className="benefits__computer--description">
            <h2>App de Escritorio</h2>
            <p>
                + Manejo de roles para acceso a información.<br/><br/>
                + Manejo y creación de reportes sobre operación.<br/><br/>
                + Formularios de llenado para operaciones terrestres,<br/>marítimas y aéreas.<br/><br/>
                + Tracking de productividad de operadores y administrativos.<br/><br/>
                + Uso intuitivo.
            </p>
        </div>
    </div>

    <div className="CtaSavings">
        <div className="CtaSavings__text">
            <h3>Accede a los ahorros que genera el uso de LogerDash en sus actividades aduaneras</h3>
            <br />
                <Link to="/forms/registro-logerdash" ><motion.button className="btn blue"
                whileHover={{ scale: 1.1 }}
                whileTap={{ scale: 0.9 }}
                >Obtener Ahorro</motion.button></Link>
        </div>
    </div>
    
    </>
    )
}

export default Logerdash
