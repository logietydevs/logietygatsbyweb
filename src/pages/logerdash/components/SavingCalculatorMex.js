import React, { useState } from 'react';
import '../logerdash.css';
import airplane from '../../../images/avion.png';
import navyship from '../../../images/barco.png';

function SavingCalculatorMex() {

    const [counter, setCounter] = useState(50);
    const [manHoursAir, setManHoursAir] = useState(200);
    const [manHoursSea, setManHoursSea] = useState(400);
    const [savingsAir, setSavingsAir] = useState(3080);
    const [savingsSea, setSavingsSea] = useState(6160);

    const handlePlus = () => {
        setCounter( counter + 50 ),
        setManHoursAir( manHoursAir  + 200 ),
        setManHoursSea( manHoursSea + 400 ),
        setSavingsAir( savingsAir + 3080 ),
        setSavingsSea( savingsSea + 6160 )
    }

    const handleMinus = () => {
        setCounter( counter - 50),
        setManHoursAir( manHoursAir - 200 ),
        setManHoursSea( manHoursSea - 400 ),
        setSavingsAir( savingsAir - 3080 ),
        setSavingsSea( savingsSea - 6160 )
        if(counter === 50 ) {(
                setCounter(50), 
                setManHoursAir(200), 
                setManHoursSea(400),
                setSavingsAir(3080),
                setSavingsSea(6160)
        )}}


    return (
        <div className="management__component">
            <div className="containerSavings">
                <div className="component">
                    <div className="component__title">
                        <h2>Emulador de Ahorro</h2>
                    </div>
                    <div className="card-1">
                        <div className="savings__container1">
                            <div className="display__number--title">
                                <p>Ingrese sus operaciones al mes:</p>
                            </div>
                            <div className="display__number--digit">
                                <p>{ counter }</p>
                            </div>

                            <div className="display__buttons--plus">
                                <button onClick={ handlePlus }>+</button>
                            </div>
                            <div className="display__buttons--minus">
                                <button onClick={ handleMinus }>-</button>
                            </div>
                        </div>
                    </div>

                    <div className="card-2">
                        <div className="savings__container2">
                            <div className="display__manHours--icons">
                                <img src={ airplane } alt="avion de carga" />
                            </div>
                            <div className="display__manHours">
                                <p>Ahorro horas hombre mensuales en AÉREO</p>
                            </div>
                            <div className="display__manHours--digit">
                                <p>{ manHoursAir }</p>
                            </div>
                            <div className="display__results">
                                <p>Obtén un ahorro al mes de</p>
                            </div>
                            <div className="display__results--digit">
                                <p> { savingsAir.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") } MXN</p>
                            </div>
                        </div>
                    </div>

                    <div className="card-3">
                        <div className="savings__container2">
                            <div className="display__manHours--icons">
                                <img src={ navyship }alt="barco naviera"/>
                            </div>
                            <div className="display__manHours">
                                <p>Ahorro horas hombre mensuales en MARÍTIMO</p>
                            </div>
                            <div className="display__manHours--digit">
                                <p>{ manHoursSea }</p>
                            </div>
                            <div className="display__results">
                                <p>Obtén un ahorro al mes de</p>
                            </div>
                            <div className="display__results--digit">
                                <p>{ savingsSea.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") } MXN</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SavingCalculatorMex
