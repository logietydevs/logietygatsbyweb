import React from 'react';
import Logerdash from '../logerdash/components/index'

function logerdashMexico() {
    return (
        <Logerdash
        heroTitle="Proteja a su equipo con el previo aduanal digitalizado"
        processAfter="Previo tradicional"
        processBefore="Previo con LogerDash"
        country="México"
        />
    )
}

export default logerdashMexico
