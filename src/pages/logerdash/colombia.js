import React from 'react';
import Logerdash from '../logerdash/components/index'

function logerdashColombia() {
    return (
        <Logerdash
        heroTitle="Proteja a su equipo con la preinspección aduanera digitalizada"
        processAfter="Preinspección tradicional"
        processBefore="Preinspección con LogerDash"
        country="Colombia"
         />
    )
}

export default logerdashColombia
