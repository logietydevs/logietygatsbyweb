import React from 'react';
import Taxer from './components/index';

function taxerMexico() {
    return (
        <Taxer
        heroTitle="Blinde su trabajo de multas ante el SAT, con el buscador de fracción arancelaria"
        heroDescription="Agilice tiempos de clasificación arancelaria y consulte estadísticas actualizadas de las aduanas de México."
        />
    )
}

export default taxerMexico
