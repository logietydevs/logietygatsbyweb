import React from 'react';
import { Link } from 'gatsby';
//images
import clasificadorResponsive from '../../../images/agente-de-aduanas-comercio-internacional-responsive.png';
import clasificador from '../../../images/agente-de-aduanas-comercio-internacional.png';
import machine from '../../../images/machine.png';
import dian from '../../../images/dian.png';
import sat from '../../../images/sat.png';
import wto from '../../../images/wto.png';
//styles
import '../taxer.css';
import { motion } from 'framer-motion';

function Taxer({heroTitle, heroDescription}) {
    return (
        <div>
        <div className="hero__taxer">
            <div className="hero__taxer--title">
                <h1>{ heroTitle }</h1>
                <h2>{ heroDescription }</h2>
                <div className="hero__image--right--responsive">
                    <img src={ clasificadorResponsive } alt="Agente Aduanal" />
                </div>
                <div className="hero__taxer--cta">
                    <Link to="https://taxer.logiety.com/" ><motion.button className="green"
                    whileHover={{ scale: 1.1 }}
                    whileTap={{ scale: 0.9 }}
                    >Utilizar Taxer</motion.button></Link>
                </div>
                <br />
                <p>Regístrese y obtenga una prueba gratuita de plan Premium</p>
                <br />
            </div>
            <div className="hero__taxer--image">
                <img src={ clasificador } alt="Clasificadores Arancelarios de Mexico" />
            </div>
        </div>

        <div className="band__benefitsTaxer">
            <div className="band__benefitsTaxer--text">
                <h2>Taxer reconoce más de Mil Millones de Mercancías</h2>
            </div>
        </div>


    <div className="demoTaxer">
        <div className="demoTaxer__description">
            <h2>Más Exacto que Google</h2>
            <h3>Toda la información que necesitas para encontrar la clasificación arancelaria en un sólo lugar</h3>
            <p>Taxer cuenta con notas explicativas, permisos, padrón de importadores, TIGIE y mucho más.</p>
        </div>

        <div className="demoTaxer__description--img">
            <img src={ machine } alt="leyes aduaneras" />
        </div>
    </div>
    <div className="demoTaxer__button">
        <div className="">
            <Link to="https://taxer.logiety.com/#/clasificar"><motion.button className="btn blue"
            whileHover={{ scale: 1.1 }}
            whileTap={{ scale: 0.9 }}
            >Quiero Clasificar</motion.button></Link>
        </div>
        <br />
    <p>Clasifica hasta 60 veces más rápido con Taxer</p>
    </div>

    <div className="authorities">
        <div className="authorities__title">
            <h3>Taxer se encuentran armonizado con los HS Codes y <br /> con la normatividad de las autoridades</h3>
        </div>
        <div className="authorities__dian">
            <img src={ dian } alt="DIAN" />
        </div>
        <div className="authorities__wto">
            <img src={ wto } alt="WTO" />
        </div>
        <div className="authorities__sat">
            <img src={ sat } alt="SAT" />
        </div>
    </div>


        </div>
    )
}

export default Taxer
