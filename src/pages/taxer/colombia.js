import React from 'react';
import Taxer from './components/index';

function taxerColombia() {
    return (
        <Taxer
        heroTitle="Blinde su trabajo de multas ante la DIAN, con el buscador de nomenclatura arancelaria"
        heroDescription="Agilice tiempos de clasificación arancelaria y consulte estadísticas actualizadas de las aduanas de Colombia."
        />
    )
}

export default taxerColombia
