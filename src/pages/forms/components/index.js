import React, { useState } from 'react';
import { push } from 'gatsby';
import { navigate } from '@reach/router';
import firebase from '../../../components/Firebase/firebase';

//images
import radar from '../../../images/testimonio-radar-holdings.png';
import chulito from '../../../images/chulito.png';
//styles
import '../forms.css';


const Forms = () => {

    const [nombre, setNombre] = useState('');
    const [correo, setCorreo] = useState('');
    const [empresa, setEmpresa] = useState('');
    const [telefono, setTelefono] = useState('');

    const [errors, setErrors] = useState({
        nombre: false,
        correo: false,
        empresa: false,
        telefono: false,
    })


    //validations for handle error
    const validationForm = (input, value) => {
        if (value === '') {
            setErrors({
                ...errors,
                [input]: true,
                isFormValid: false,
            })
        } else {
            setErrors({
                ...errors,
                [input]: false,
                isFormValid: true,
            })
        }
    };


    const onSubmit = e => {
    e.preventDefault()
    firebase
        .firestore()
        .collection("clientsContact")
        .add({
            nombre,
            correo,
            empresa,
            telefono,
            })
        .then(() =>
        navigate("/success/register", { replace: true }),
        setNombre(""), setCorreo(""), setEmpresa(""), setTelefono(""), push())
    }

    return (
    <div className="forms">
        <div className="forms__image">
            <img src={ radar } alt="Testimonio Radar" />
        </div>
        <div className="forms__card">
            <div className="forms__card--content">
                <h2>Activación 15 días Gratuitos de LogerDash</h2>
                <div className="no_payment_advices">
                    <p><img src={ chulito } style={{width: "16px", height: "16px"}} /> No requiere tarjeta
                        de crédito, ni pagos anticipados.</p>
                    <p><img src={ chulito } style={{width: "16px", height: "16px"}} /> Servicio en línea. No requiere instalación de software.</p>
                </div>
                <br/>



                <form onSubmit={ onSubmit }>

                    <label>Nombre Completo del Solicitante *</label>

                    <input 
                    className="input__request" 
                    onChange={ e => setNombre(e.currentTarget.value) } 
                    type="text" 
                    placeholder="Ejem. Pedro Navajas" 
                    onBlur={ e => validationForm(e.target.name, e.target.value) }
                    name="nombre"
                    value={nombre}
                    required />

                    <label>Correo Electrónico *</label>
                    <input className="input__request" 
                    onChange={ e => setCorreo(e.currentTarget.value) } 
                    type="email" 
                    placeholder="Ejem. pedro@rubenblades.com"
                    onBlur={e => validationForm(e.target.name, e.target.value) }
                    name="correo_electronico"
                    value={correo}
                    required />

                    <label>Nombre de la Agencia / Negocio *</label>
                    <input className="input__request" 
                    onChange={ e => setEmpresa(e.target.value) } 
                    type="text" 
                    placeholder="Agencia Aduanal Blades" 
                    onBlur={e => validationForm(e.target.name, e.target.value) }
                    name="empresa"
                    value={empresa}
                    required />

                    <label>Contacto del Solitante *</label>
                    <input className="input__request" 
                    onChange={ e => setTelefono(e.target.value) } 
                    type="text" 
                    placeholder="Teléfono de Oficina o Celular / Móvil"
                    onBlur={e => validationForm(e.target.name, e.target.value) }
                    name="telefono" 
                    value={telefono}
                    required />
        
                     {/* <Link to="/success/register">   */}
                    <button type="submit" className="btn1 green" value="Enviar">
                    Enviar
                    </button>
                    <p style={{ fontSize: "16px" }}></p>
                    {/* </Link>  */}


                    <input 
                    className="forms__terms" 
                    type="checkbox"
                    required /> 
                    Acepto los términos ycondiciones y las Políticas de Privacidad
                    <p style={{ fontSize: "14px" }}>* Campos obligatorios</p>
                </form>
            </div>
        </div>
    </div>
    )
}

export default Forms
