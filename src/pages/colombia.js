import React from 'react';
import Home from "./Home/index";
import SEO from "../components/seo";
import serpomar from '../images/serpomar-icon.png';
import sudeco from '../images/sudeco-icon.png';
import hubemar from '../images/hubemar-icon.png';
import sescaribe from '../images/ses-caribe-icon.png';
import sprc from '../images/sprc-icon.png';

const colombia = () => {
    return (
    <>
    <SEO title="Logiety Colombia" />
    <Home
      heroTitle="Digitalice oportunidades en la inspección aduanal y clasificación arancelaria"
      country="Colombia"
      logerProductTitle="INSPECCIÓN ADUANERA DIGITALIZADO Y EN LA NUBE"
      logerProductDescription="Preinspección e inspección de mercancías en una sola plataforma."
      alt1="agencia aduanera Serpomar"
      client1={serpomar}
      alt2="Sociedad Portuaria Regional de Cartagena"
      client2={sprc}
      alt3="agencia aduanera Hubemar"
      client3={hubemar}
      alt4="Operador Logistico Sescaribe"
      client4={sescaribe}
      alt5="agencia aduanera Sudeco"
      client5={sudeco}
    />
  </>
    )
}

export default colombia
