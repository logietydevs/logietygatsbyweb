import React from "react";
import Home from "./Home/index";
import SEO from "../components/seo";
import cmma from "../images/cmma-icon.png";
import grupoEi from '../images/grupo-ei-icon.png';
import grupoRadar from '../images/radar-icon.png';
import aduanet from '../images/aduanet-icon.png';
import logietrade  from '../images/logytrade-icon.png';

const IndexPage = () => (
  <>
    <SEO title="Logiety México" />
    <Home
      heroTitle="Digitalizamos el despacho aduanero y clasificación arancelaria"
      country="México"
      logerProductTitle="EL PREVIO ADUANAL DIGITALIZADO Y EN LA NUBE"
      logerProductDescription="Administración de previos, revisiones de mercancías en una sola plataforma."
      alt1="agencia aduanal cmma"
      client1={cmma}
      alt2="agencia aduanal Grupo EI"
      client2={grupoEi}
      alt3="agencia aduanal Grupo Radar"
      client3={grupoRadar}
      alt4="Servicios Digitales Aduanet"
      client4={aduanet}
      alt5="agencia aduanal Logitrade"
      client5={logietrade}
    />
  </>
)

export default IndexPage
