import React from 'react'
import { Link } from 'gatsby';
import './blog.css';

function Sidebar() {
    return (
    <>
        <div className="blog__categories">
            <ul>
                <li><Link to="/">Innovación</Link></li>
                <li><Link to="/">Logística</Link></li>
                <li><Link to="/">Clasificación</Link></li>
                <li><Link to="/">Blockchain</Link></li>
            </ul>
        </div>
    </>
    )
}

export default Sidebar
