import React from 'react'
import { Link, graphql, useStaticQuery } from 'gatsby';
import './blog.css';

// sort: {fields: published, order: ASC}, 

function OlderPost() {

    const data = useStaticQuery(graphql`
    {
      allPost (sort: {fields: postNumber, order: DESC} skip: 4)  {
        edges {
          node {
            id
            postNumber
            date
            altImage1
            imgUrl1
            title
            autor {
              nombre
            }
          }
        }
      }
    }
  `)

    return (
        <>
        <div className="blog__latest">
        <h3>Posts de Interés</h3>
        <hr />
        { data.allPost.edges.map(edge => (
        <div key={edge.node.id} className="blog__latest--headers">
            <Link to={`/post/${edge.node.id}`}><h3>{ edge.node.title }</h3></Link>
            <p>Contenido</p>
            <img src={ edge.node.imgUrl1 } alt={ edge.node.altImage1 } />
            <hr />
        </div>
        ))}
        </div>
        </>
    )
}

export default OlderPost
