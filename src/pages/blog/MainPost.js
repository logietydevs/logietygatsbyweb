import React from 'react';
import { Link, graphql, useStaticQuery } from 'gatsby';

//images
import daniel from '../../images/daniel.png'
import ricardo from '../../images/ricardo.jpg'
//styles
import './blog.css';


const MainPost = () => {

    const data = useStaticQuery(graphql`
    {
      allPost (sort: {fields: postNumber, order: DESC}, limit : 1)  {
        edges {
          node {
            id
            postNumber
            categoria
            date
            altImage1
            title
            imgUrl1
            autor {
              nombre
            }
          }
        }
      }
    }
  `)

  const autorSelection = data.allPost.edges[0].node.autor.nombre
  

    return (
        <div  className="blog__main">
        { data.allPost.edges.map(edge => (
            <div key={edge.node.id} className="blog__main--title">
                <Link to={`/post/${edge.node.id}`}><h3>{ edge.node.title }</h3>
                </Link>
                <p> { edge.node.autor.nombre } </p>

                { autorSelection === "Daniel Alarcón" ?
                <img src={ daniel } alt="Daniel Alarcón Foto" style={{ maxWidth: "2rem", borderRadius: "50%" }} />
                :
                <img src={ ricardo } alt="Ricardo Sordo Foto" style={{ maxWidth: "2rem", borderRadius: "50%" }} />
                }
                <br/><p>{ edge.node.date }</p>
                <img src={ edge.node.imgUrl1 } alt={ edge.node.altImage1 } />
            </div>
        ))}
        </div>
    )
}

export default MainPost
