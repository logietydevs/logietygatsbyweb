import React from 'react';
import { Link, graphql, useStaticQuery } from 'gatsby';
import './blog.css';

// sort: {fields: published, order: ASC},

const SidePost = () => {

    const data = useStaticQuery(graphql`
    {
      allPost (sort: {fields: postNumber, order: DESC}, skip : 1, limit : 3,)  {
        edges {
          node {
            id
            postNumber
            date
            title
            autor {
              nombre
            }
          }
        }
      }
    }
  `)

    return (
        <div className="fort">
        { data.allPost.edges.map(edge => (
        <div key={edge.node.id} >
            <Link to={`/post/${edge.node.id}`}><h3>{ edge.node.title }</h3></Link>
            <p> { edge.node.autor.nombre } <br/>{ edge.node.date }</p>
            <hr />
        </div>
         ))}
        </div>
    )
}

export default SidePost

