import React from 'react';
import { Link } from 'gatsby';
import SEO from '../../components/seo'
//images
import taxerCard from '../../images/taxer.webp';
import logerDashCard from '../../images/logerdash.webp';
import chulito from '../../images/chulito.png';
import componentIndex from '../../images/component-index.png';
import testimonies from '../../images/test-testimonies.png';
import testimoniesRespons from '../../images/testimonios-clientes-responsivo.png';
import apiService from '../../images/api-service.png';
import agentesAduanales from '../../images/agentes-aduanales.png';
import agentesResponsive from '../../images/agentes-aduanales-resposivo.png';
//styles
import "./home.css";
import { motion } from 'framer-motion';


function Home(props) {
    return (
        <>
        <div className="hero">
        <div className="hero__title">
            <h1>{ props.heroTitle }</h1>
            <h2>Software para comercio exterior, <b>fácil, rápido y listo para utilizarse.</b></h2>
            <div className="hero__image--right--responsive ">
                <img src={ agentesResponsive } alt="Agente Aduanal" />
            </div>
        </div>
        <div className="hero__image--right">
            <img src={ agentesAduanales }
            alt="Agentes aduanales y clasificadores del comercio exterior" />
        </div>
    </div>
        <div className="products">
        <div className="products__title">
            <h2>Haga suya la mejor tecnología del mercado</h2>
            <p>Herramientas amigables para empresas y profesionales de alto rendimiento.</p>
        </div>
        <div className="products__taxer">
            <img src={ taxerCard } alt="Taxer clasificador arancelario" />
            <div className="products__description--taxer">
                <h3>EL BUSCADOR DE CLASIFICACIÓN ARANCELARIA EN LÍNEA</h3>
                <hr />
                <p>Clasificación asistida con inteligencia artificial y estadísticas de comercio exterior.</p>
            </div>
            <div className="products__taxer--button">
            { props.country === "México" ?
                <Link to="/taxer/mexico" className="btn blue">Conocer más</Link> :
                <Link to="/taxer/colombia" className="btn blue">Conocer más</Link>
            }
            </div>
        </div>
        <div className="products__inspections">
            <img src={ logerDashCard } alt="LogerDash el control aduanal" />
            <div className="products__description--inspections">
                <h3>{ props.logerProductTitle }</h3>
                <hr />
                <p>{ props.logerProductDescription }</p>
                <br/>
            </div>
            <div className="products__inspections--button">
            { props.country === "México" ?
                <Link to="/logerdash/mexico" className="btn blue">Conocer más</Link> :
                <Link to="/logerdash/colombia" className="btn blue">Conocer más</Link>
            }
            </div>
        </div>
        </div>

        <div className="clients">
        <div className="clients__title">
            <h2>Increíbles empresas han confiando en nuestra pasión por el comercio exterior.</h2>
        </div>
        <div className="clients__img--cmma">
            <img src={ props.client1 } alt={props.alt1} />
        </div>

        <div className="clients__img--ei">
            <img src={ props.client2 } alt={props.alt2} />
        </div>

        <div className="clients__img--radar">
            <img src={ props.client3 } alt={props.alt3} />
        </div>

        <div className="clients__img--aduanet">
            <img src={ props.client4 } alt={props.alt4} />
        </div>

        <div className="clients__img--logitrade">
            <img src={ props.client5 } alt={props.alt5} />
        </div>
    </div>

    <div className="itExpenses">
        <div className="itExpenses__description">
            <h2>Una rápida instalación</h2>
            <p>Los servicios de Logiety se encuentran disponibles para cualquier equipo y dispositivo en el mundo.</p>
            <p><img src={ chulito } style={{ width : "20px", height: "20px" }}/> Configuración en
                instalación de herramientas en minutos.</p>
            <p><img src={ chulito }  style={{ width : "20px", height: "20px" }} /> Soporte técnico
                personalizado para instalación inmediata.</p>
            <p><img src={ chulito }  style={{ width : "20px", height: "20px" }} /> Capacitación premium en
                línea.</p>
            <p><img src={ chulito }  style={{ width : "20px", height: "20px" }} /> Servicio al cliente 24/7,
                365 día al año.</p>
        </div>
        <div className="itExpenses__image">
            <img src={ componentIndex } alt="Gastos en desarrollo de software" />
        </div>
    </div>

    <div className="evaluations">
        <div className="evaluations__title">
            <h2>Los clientes dicen de nuestros productos</h2>
        </div>
        <div className="evaluations__image">
            <img src={ testimonies } alt="Testimonios de clientes satisfechos" />
        </div>
        <div className="evaluations__image--responsive">
            <img src={ testimoniesRespons } alt="Más testimonios de clientes satisfechos" />
        </div>
    </div>


    <div className="api">
        <div className="api__title">
            <h2>¿Cuenta con desarrollo propio o utiliza sistemas de terceros? </h2>
            <p>Contamos con servicios API REST y un gran equipo técnico para rápidas integraciones con su negocio</p>
            <div style={{ padding : "60px" }}>
                <Link to="/forms/registro-logerdash"><motion.button className="btn blue"
                whileHover={{ scale: 1.1 }}
                whileTap={{ scale: 0.9 }}
                >Contactar Departamento TI</motion.button></Link>
            </div>
        </div>
        <div className="api__img--service">
            <img src={ apiService } alt="Conexión a software de terceros" />
        </div>
    </div>
        </>

    )
}

export default Home
