import React from 'react';
// Components
import SideNavbar from './blog/SideNavbar';
import MainPost from './blog/MainPost';
import SidePost from './blog/SidePost';
import OlderPost from './blog/OlderPost';
//styles
import './blog/blog.css';



const BlogContent = () => {

    return (
    <>
    <div  className="blog">
    <SideNavbar />
    <MainPost />
    <SidePost />
    <OlderPost />
    </div>
    </>
    )
}

export default BlogContent
