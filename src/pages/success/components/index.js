import React from 'react';
import { Link } from 'gatsby';
//images
import peopleThanks from '../../../images/success.jpg';
//styles
import '../sucess.css';

function Success({flag}) {
    return (
    <div className="result">
        <div className="result__message">
            <img src={ peopleThanks } alt="exito" />
            <h2>Su mensaje de solicitud ha sido enviado exitosamente!</h2>
            <p>En breve el equipo de Logiety se pondrá en contacto contigo.</p>
            <br />
            { flag === "México" ?
            <Link to="/" className="btn blue">ir a Inicio</Link>
            :
            <Link to="/colombia" className="btn blue">ir a Inicio</Link>
            }
        </div>
    </div>
    )
}

export default Success
