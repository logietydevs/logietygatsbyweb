import React from 'react';
import { Link } from 'gatsby';
import SideNavbar from '../pages/blog/SideNavbar';
//styles
import '../pages/blog/blog.css';

import daniel from '../images/daniel.png'
import ricardo from '../images/ricardo.jpg'


const postTemplate = (props) => {

    const contentDivided1 = props.pageContext.content1;
    const contentDivided2 = props.pageContext.content2;
    const autorSelection = props.pageContext.autor.nombre;



    return (
    <>
    <div  className="blog">
    <SideNavbar />
        <div className="blog__detail">
            <div className="blog__detail--description">
            <h3>{ props.pageContext.title }</h3>
            { autorSelection === "Daniel Alarcón" ?
            <p>{ props.pageContext.autor.nombre } <img src={ daniel } alt="Daniel Alarcón Foto" style={{ maxWidth: "2rem", borderRadius: "50%" }} /><br/>{ props.pageContext.published }</p>
            :
            <p>{ props.pageContext.autor.nombre } <img src={ ricardo } alt="Ricardo Sordo Foto" style={{ maxWidth: "2rem", borderRadius: "50%" }} /><br/>{ props.pageContext.published }</p>
            }
            <p>{ props.pageContext.readTime }</p>
            <img src={ props.pageContext.imgUrl1 } alt={ props.pageContext.altImage1 } />

            {
            contentDivided1.split ('<br />').map ((item, i) => <p key={i}>{item}</p>)
            }

            <img src={ props.pageContext.imgUrl2 } alt={ props.pageContext.altImage2 } />
            <p className="quotes">{ props.pageContext.quotes }</p>

            {
            contentDivided2.split ('<br />').map ((item, i) => <p key={i}>{item}</p>)
            }

        </div>
        <div className="blog__detail--category">
            <h4> { props.pageContext.categoria } </h4>
            <hr />
        </div>
    </div>

    <div className="blog__latest">
        <Link to="/blog" className="green" style={{ fontWeight: "500" }}>Regresar al Blog</Link>
    </div>
    </div>
    </>
    )
}

export default postTemplate
