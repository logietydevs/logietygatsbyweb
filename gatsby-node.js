const path = require('path');
// const { paginate } = require('gatsby-awesome-pagination');

exports.createPages = ({graphql, actions}) => {
    const {createPage} = actions;
    const postTemplate = path.resolve('src/templates/postTemplate.js');
  
    return graphql(`
    {
        allPost {
          edges {
            node {
              id
              altImage1
              altImage2
              categoria
              content1
              content2
              imgUrl1
              imgUrl2
              date
              quotes
              postNumber
              title
              autor {
                nombre
              }
            }
          }
        }
      }

    `).then((results) =>  {
      if(results.errors){
        throw results.errors;
      }

      results.data.allPost.edges.forEach(post =>{
        createPage({
            path: `/post/${post.node.id}`,
            component: postTemplate,
            context: post.node
        })
    });

  })

}